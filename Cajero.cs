﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrón_Diseño_Builder
{
    class Cajero
    {
        public ComidapNiños comidapniños;
        public ComidaNiños comidaNiños;

        public Cajero(ComidapNiños comidapniños)
        {
            this.comidapniños = comidapniños;
        }

        public Cajero(ComidaNiños comidaNiños)
        {
            this.comidaNiños = comidaNiños;
        }

        public Comida getComida()
        {
            return this.comidapniños.getComida();
        }

        public void hacercomida()
        {
            this.comidapniños.buildHamburguesa();
            this.comidapniños.buildAdicional();
            this.comidapniños.buildBebida();
            this.comidapniños.buildJuguete();
        }
    }
}