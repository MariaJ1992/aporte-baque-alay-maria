﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrón_Diseño_Builder
{
    class Cliente
    {
        public Cajero cajero;

        public Cliente()
        {
            cajero = new Cajero(new ComidaNiños());
        }

        public Comida getComida()
        {
            return cajero.getComida();
        }

        public void pedircomida()
        {
            cajero.hacercomida();
        }
    }
}