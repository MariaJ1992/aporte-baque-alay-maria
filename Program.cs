﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrón_Diseño_Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            Cliente cliente = new Cliente();
            cliente.pedircomida();

            Comida meal = cliente.getComida();

            Console.WriteLine("Hamburguesa: " + meal.getHamburguesa());
            Console.WriteLine("Bebida: " + meal.getBebida());
            Console.WriteLine("Adicional: " + meal.getAdicional());
            Console.WriteLine("Juguete: " + meal.getJuguete());
        }
    }
}
