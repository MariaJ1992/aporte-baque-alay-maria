﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrón_Diseño_Builder
{
    class ComidaNiños
    {
        public Comida comida;

        public ComidaNiños()
        {
            this.comida = new Comida();
        }

          public void buildHamburguesa()
        {
            comida.setHamburguesa("Hamburguesa de Carne");
        }

           public void buildBebida()
        {
            comida.setBebida("Zumo de Naranja");
        }

           public void buildAdicional()
        {
            comida.setAdicional("Deditos de Carne");
        }

           public void buildJuguete()
        {
            comida.setJuguete("Cubito ");
        }

            public Comida getComida()
        {
            return comida;
        }
    }
}
