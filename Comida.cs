﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrón_Diseño_Builder
{
    class Comida
    {
        public String hamburguesa;
        public String bebida;
        public String adicional;
        public String juguete;

        public String getHamburguesa()
        {
            return hamburguesa;
        }

        public void setHamburguesa(String hamburguesa)
        {
            this.hamburguesa = hamburguesa;
        }

        public String getBebida()
        {
            return bebida;
        }

        public void setBebida(String bebida)
        {
            this.bebida = bebida;
        }

        public String getJuguete()
        {
            return juguete;
        }

        public void setJuguete(String juguete)
        {
            this.juguete = juguete;
        }

        public String getAdicional()
        {
            return adicional;
        }

        public void setAdicional(String adicional)
        {
            this.adicional = adicional;
        }
    }
}