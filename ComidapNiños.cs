﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrón_Diseño_Builder
{
    public interface ComidapNiños
    {
        public void buildHamburguesa();
        public void buildBebida();
        public void buildAdicional();
        public void buildJuguete();
        public Comida getComida();
    }
}